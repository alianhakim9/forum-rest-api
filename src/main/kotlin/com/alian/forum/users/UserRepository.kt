package com.alian.forum.users

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface UserRepository : JpaRepository<Users, Long> {
    @Query("SELECT u FROM Users u WHERE u.username=?1")
    fun findByUsername(username: String): Optional<Users>

    @Query("SELECT p FROM Users p WHERE p.password=?1")
    fun checkPassword(password : String) : Optional<Users>
}