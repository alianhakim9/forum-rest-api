package com.alian.forum.question

import org.springframework.data.jpa.repository.JpaRepository

interface QuestionService : JpaRepository<Question, Long> {
}