package com.alian.forum.users

import javax.persistence.*


@Entity
data class Users(
    @Id
    @SequenceGenerator(
        name = "user_sequence",
        sequenceName = "user_sequence",
        allocationSize = 1
    )
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "user_sequence"
    )
    val id: Long,

    @Column(unique = true)
    val username: String,

    @Column(unique = true)
    val email: String,
    val password: String,

    @Column(name = "full_name")
    val fullName: String
)
