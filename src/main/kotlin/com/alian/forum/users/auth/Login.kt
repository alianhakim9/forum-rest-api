package com.alian.forum.users.auth

data class Login(
    val username: String,
    val password: String
)
