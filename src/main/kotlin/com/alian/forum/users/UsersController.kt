package com.alian.forum.users

import com.alian.forum.users.response.UserResponse
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/")
class UsersController(private val repository: UserRepository) {

    private val service = UserService(repository)

    @GetMapping("users")
    fun getAllUser(): UserResponse = service.getAll()

    @GetMapping("users/{id}")
    fun getUserById(@PathVariable("id") id: Long): Users = service.getById(id)

    @PostMapping("users")
    fun addUser(@RequestBody users: Users) = service.add(users)

    @DeleteMapping("users/{id}")
    fun deleteById(@PathVariable("id") id: Long): String = service.deleteById(id)

    @PostMapping("users/login")
    fun login(@RequestParam username: String, @RequestParam password: String): String =
        service.login(username, password)

}