package com.alian.forum.users.response

import com.alian.forum.users.Users

data class UserResponse(
    val status: Boolean,
    val count: Int,
    val users: MutableList<Users>
)
