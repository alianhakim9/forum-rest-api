package com.alian.forum.users

import com.alian.forum.users.response.UserResponse
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

class UserService(private val repository: UserRepository) {

    fun add(users: Users) {
        if (repository.findById(users.id).isEmpty) {
            val passwordEncoder = BCryptPasswordEncoder()
            val hashedPassword = passwordEncoder.encode(users.password)
            val newUser = Users(users.id, users.username, users.email, hashedPassword, users.fullName)
            repository.save(newUser)
        } else {
            throw IllegalArgumentException("users already exists")
        }
    }

    fun getAll(): UserResponse {
        val user = repository.findAll()
        return UserResponse(true, user.size, user)
    }

    fun getById(id: Long): Users {
        val user = repository.findById(id)
        if (user.isPresent) {
            return user.get()
        }
        throw IllegalArgumentException("user with id $id not found")
    }

    fun deleteById(id: Long): String {
        val user = repository.findById(id)
        if (user.isPresent) {
            repository.deleteById(id)
            return "Deleted Successfully"
        }
        throw IllegalArgumentException("user with id $id not found")
    }

    fun login(username: String, password: String): String {
        val user = repository.findByUsername(username)
        if (user.isPresent) {
            val encoder = BCryptPasswordEncoder()
            if (encoder.matches(password, user.get().password)) {
                println("login success")
                return "Login success"
            } else {
                println("wrong password")
                throw IllegalArgumentException("wrong password")
            }
        } else {
            println("user not exists")
            throw IllegalArgumentException("user not exists")
        }
    }
}