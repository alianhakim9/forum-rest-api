package com.alian.forum.question

import com.alian.forum.category.Category
import com.alian.forum.users.Users
import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.*


@Entity
data class Question(

    @Id
    @SequenceGenerator(
        name = "question_sequence",
        sequenceName = "question_sequence",
        allocationSize = 1
    )
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "question_sequence"
    )
    val id: Long,

    @ManyToOne
    @JoinColumn(name = "user_id")
    val users: Users,

    @ManyToOne
    @JoinColumn(name = "category_id")
    val category: Category,


    val question: String,

    @Column(name = "question_desc", columnDefinition = "TEXT")
    val questionDesc: String,

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    val createdAt: Date
)
